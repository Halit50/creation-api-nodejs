- initialisation de son projet avec npm avec la commande: npm init -y (le -y sert à repondre par oui par défaut à toutes les questions posées) 

- installation d'express et nodemon avec la commande: npm i -s express nodemon (i pour install et -s pour save)

- dans le package.json, remplacer le "test" par "start" dans la rubrique "scripts" puis saisir "nodemon index.js"

- lancer le serveur:
const express = require('express');
const app = express();

app.listen(5500, () => console.log('Server started: 5500'));

- créer la base de données MongoDB puis installer le package Mongoose avec la commande: npm i -s mongoose