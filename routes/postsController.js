const express = require('express');
const router = express.Router();
// premet de récupérer l'id des objets qui se trouvent dans ma bdd
const ObjectID = require('mongoose').Types.ObjectId;

const { PostsModel } = require('../models/postsModel');

// Récupérer les données de ma bdd
router.get('/', (req, res) => {
    PostsModel.find((err, docs) => {
        if (!err) res.send(docs);
        else console.log('Error to get data:' + err);
    })
})

// Créer des données dans ma bdd
router.post('/', (req, res) => {
    const newRecord = new PostsModel({
        author: req.body.author,
        message: req.body.message
    });

    newRecord.save((err, docs) => {
        if (!err) res.send(docs);
        else console.log('Error creating new data:' + err);
    })
})

// Mettre à jour des données dans ma bdd
router.put('/:id', (req,res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID unknow:" + req.params.id)
    
    const updateRecord = {
        author: req.body.author,
        message: req.body.message
    };

    PostsModel.findByIdAndUpdate(
        req.params.id,
        { $set: updateRecord},
        { new: true },
        (err, docs) => {
            if (!err) res.send(docs);
            else console.log("Update error" + err)
        }
    )
})

// supprimer une donnée de ma bdd
router.delete('/:id', (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send('ID unknow:' + req.params.id)
    
    PostsModel.findByIdAndRemove(
        req.params.id,
        (err, docs) => {
            if (!err) res.send(docs);
            else console.log('Delete error:' + err);
        }
    )
})

module.exports = router;