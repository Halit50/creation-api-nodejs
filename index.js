const express = require('express');
const app = express();
require('./models/dbConfig.js');
const postsRoutes = require('./routes/postsController');
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(bodyParser.json());
// autorisation de l'utilisation de l'API seulement sur le site indiqué
app.use(cors({origin: 'https://cdpn.io'}));
app.use('/posts', postsRoutes);

app.listen(5500, () => console.log('Server started: 5500'));